import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyBFBOlXY9lhJfVAGfxmN5G1cosTMAeAI9c',
  authDomain: 'mezcalpormexico.firebaseapp.com',
  databaseURL: 'https://mezcalpormexico.firebaseio.com',
  projectId: 'mezcalpormexico',
  storageBucket: 'mezcalpormexico.appspot.com',
  messagingSenderId: '128427142625',
  appId: '1:128427142625:web:d5131056c40a52377d76a0',
  measurementId: 'G-3G0GX495TD'
}

firebase.initializeApp(firebaseConfig)

export const db = firebase.firestore()
