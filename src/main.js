import Vue from 'vue'
import App from './App.vue'
import VueScrollTo from 'vue-scrollto'
// import * as VueGoogleMaps from 'vue2-google-maps'
import './registerServiceWorker'
import './assets/scss/main.scss'

Vue.config.productionTip = false
Vue.use(VueScrollTo)
// Vue.use(VueGoogleMaps, {
//   load: {
//     key: 'AIzaSyBhLeo_uonpdYchCZp3hDDvYtxWmrDuIQU'
//   }
// })

new Vue({
  render: h => h(App)
}).$mount('#app')
